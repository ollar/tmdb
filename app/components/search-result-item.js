import Component from "@glimmer/component";
import { inject as service } from "@ember/service";
import { readOnly } from "@ember/object/computed";

export default class SearchResultItemComponent extends Component {
    @service tmdbConfiguration;

    @readOnly("tmdbConfiguration.backdropPaths") backdropPaths;
    @readOnly("tmdbConfiguration.posterPaths") posterPaths;
}
