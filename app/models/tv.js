import Model, { attr } from "@ember-data/model";

export default class TvModel extends Model {
    @attr backdrop_path;
    @attr created_by;
    @attr episode_run_time;
    @attr first_air_date;
    @attr genres;
    @attr homepage;
    @attr in_production;
    @attr languages;
    @attr last_air_date;
    @attr last_episode_to_air;
    @attr name;
    @attr next_episode_to_air;
    @attr networks;
    @attr number_of_episodes;
    @attr number_of_seasons;
    @attr origin_country;
    @attr original_language;
    @attr original_name;
    @attr overview;
    @attr popularity;
    @attr poster_path;
    @attr production_companies;
    @attr seasons;
    @attr status;
    @attr type;
    @attr vote_average;
    @attr vote_count;
    @attr media_type;
}
