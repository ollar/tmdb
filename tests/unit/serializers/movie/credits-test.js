import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Serializer | movie/credits', function(hooks) {
  setupTest(hooks);

  // Replace this with your real tests.
  test('it exists', function(assert) {
    let store = this.owner.lookup('service:store');
    let serializer = store.serializerFor('movie/credits');

    assert.ok(serializer);
  });

  test('it serializes records', function(assert) {
    let store = this.owner.lookup('service:store');
    let record = store.createRecord('movie/credits', {});

    let serializedRecord = record.serialize();

    assert.ok(serializedRecord);
  });
});
