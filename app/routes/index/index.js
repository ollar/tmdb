import Route from "@ember/routing/route";
import { inject as service } from "@ember/service";
import { hash } from "rsvp";

export default class IndexIndexRoute extends Route {
    @service trending;
    @service tmdbConfiguration;

    model() {
        return hash({
            movie: this.trending.fetch("movie", "day"),
            tv: this.trending.fetch("tv", "day")
            // person: this.trending.fetch("person", "day")
        });
    }
}
