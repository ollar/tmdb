import ApplicationAdapter from "../application";

export default class MovieRecommendationsAdapter extends ApplicationAdapter {
    buildURL(modelName, id /*, snapshot, requestType, query */) {
        let prefix = this.urlPrefix();
        return `${prefix}/movie/${id}/recommendations`;
    }
}
