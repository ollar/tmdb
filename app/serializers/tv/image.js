import ApplicationSerializer from "../application";

export default class MovieImageSerializer extends ApplicationSerializer {
    attrs = {
        backdrops: { embedded: "always" },
        posters: { embedded: "always" }
    };
}
