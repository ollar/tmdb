import { helper } from "@ember/component/helper";

function formatDuration(minutes, res = "") {
    let hours = Math.floor(minutes / 60);
    if (hours >= 1) {
        res += `${hours} h `;
        return formatDuration(minutes % 60, res);
    }
    return (res += `${minutes} m`);
}

export default helper(function runtime(params /*, hash*/) {
    return formatDuration(params[0]);
});
