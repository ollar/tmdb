import Component from "@glimmer/component";
import { inject as service } from "@ember/service";
import { readOnly } from "@ember/object/computed";

export default class CastBlockComponent extends Component {
    @service tmdbConfiguration;
    @readOnly("tmdbConfiguration.profilePaths") profilePaths;

    get noPhotoSrc() {
        return this.args.data.gender === 1
            ? "images/woman.svg"
            : "images/man.svg";
    }
}
