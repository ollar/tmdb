import Model, { attr } from "@ember-data/model";

export default class MovieModel extends Model {
    @attr adult;
    @attr backdrop_path;
    @attr belongs_to_collection;
    @attr budget;
    @attr genres;
    @attr homepage;
    @attr imdb_id;
    @attr original_language;
    @attr original_title;
    @attr overview;
    @attr popularity;
    @attr poster_path;
    @attr production_companies;
    @attr production_countries;
    @attr release_date;
    @attr revenue;
    @attr runtime;
    @attr spoken_languages;
    @attr status;
    @attr tagline;
    @attr title;
    @attr video;
    @attr vote_average;
    @attr vote_count;
    @attr media_type;

    get name() {
        return this.title;
    }
}
