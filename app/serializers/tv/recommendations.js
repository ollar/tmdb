import ApplicationSerializer from "../application";

export default class MovieRecommendationsSerializer extends ApplicationSerializer {
    attrs = {
        results: { embedded: "always" }
    };

    normalizeResponse(store, primaryModelClass, payload, id, requestType) {
        return super.normalizeResponse(
            store,
            primaryModelClass,
            { id, ...payload },
            id,
            requestType
        );
    }
}
