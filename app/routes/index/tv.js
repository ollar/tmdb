import Route from "@ember/routing/route";
import { hash } from "rsvp";

export default class IndexTvRoute extends Route {
    model({ tv_id }) {
        return hash({
            tv_id,
            info: super.model(...arguments)
            // credits: this.store.findRecord("tv/credits", tv_id),
            // images: this.store.findRecord("tv/image", tv_id),
            // videos: this.store.findRecord("tv/video", tv_id),
            // similar: this.store.findRecord("tv/similar", tv_id),
            // recommendations: this.store.findRecord(
            //     "tv/recommendations",
            //     tv_id
            // )
        });
    }
}
