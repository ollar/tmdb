import Component from "@glimmer/component";
import { inject as service } from "@ember/service";
import { readOnly } from "@ember/object/computed";

export default class TrendingBlockItemComponent extends Component {
    @service tmdbConfiguration;

    @readOnly("tmdbConfiguration.images") imagesConfigs;
    @readOnly("imagesConfigs.secure_base_url") baseUrl;
    @readOnly("imagesConfigs.backdrop_sizes") imageSizes;

    get name() {
        return this.args.data.title || this.args.data.name;
    }

    get imagesPaths() {
        return {
            s: `${this.baseUrl}${this.imageSizes[1]}${this.args.data.backdrop_path}`,
            m: `${this.baseUrl}${this.imageSizes[2]}${this.args.data.backdrop_path}`,
            l: `${this.baseUrl}${this.imageSizes[3]}${this.args.data.backdrop_path}`
        };
    }
}
