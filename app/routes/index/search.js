import Route from "@ember/routing/route";
import { inject as service } from "@ember/service";

export default class IndexSearchRoute extends Route {
    @service search;

    model() {
        return this.search.results;
    }
}
