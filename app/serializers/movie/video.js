import ApplicationSerializer from '../application';

export default class MovieVideoSerializer extends ApplicationSerializer {
    attrs = {
        results: { embedded: 'always' }
    }
}
