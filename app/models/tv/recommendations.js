import Model, { hasMany } from "@ember-data/model";

export default class TvRecommendationsModel extends Model {
    @hasMany("tv", {
        async: false
    })
    results;
}
