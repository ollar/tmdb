import Model, { attr } from "@ember-data/model";

export default class PersonModel extends Model {
    @attr birthday;
    @attr known_for_department;
    @attr deathday;
    @attr name;
    @attr also_known_as;
    @attr gender;
    @attr biography;
    @attr popularity;
    @attr place_of_birth;
    @attr profile_path;
    @attr adult;
    @attr imdb_id;
    @attr homepage;
    @attr media_type;
}
