import Model, { hasMany } from "@ember-data/model";

export default class MovieImageModel extends Model {
    @hasMany("embed/backdrop", {
        async: false
    })
    backdrops;
    @hasMany("embed/poster", {
        async: false
    })
    posters;
}
