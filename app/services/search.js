import Service, { inject as service } from "@ember/service";
import { tracked } from "@glimmer/tracking";

export default class SearchService extends Service {
    @service store;

    @tracked results = [];

    get appAdapter() {
        return this.store.adapterFor("application");
    }

    get prefix() {
        return `${this.appAdapter.host}/${this.appAdapter.namespace}`;
    }

    get url() {
        return `${this.prefix}/search/multi`;
    }

    fetch(query = "") {
        let _query = query.trim();
        if (_query.length === 0) {
            this.results = [];
            return;
        }

        return this.appAdapter
            .ajax(this.url, "GET", {
                data: { query: _query }
            })
            .then(resp => {
                this.results = [];
                resp.results.forEach(m => {
                    let model = this.store.push(
                        this.store.normalize(m.media_type, m)
                    );
                    this.results.pushObject(model);
                });

                return this.results;
            });
    }
}
