import Model, { attr } from "@ember-data/model";

export default class CrewModel extends Model {
    @attr credit_id;
    @attr department;
    @attr gender;
    @attr job;
    @attr name;
    @attr profile_path;
}
