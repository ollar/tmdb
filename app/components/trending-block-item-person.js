import TrendingBlockItemComponent from "./trending-block-item";

export default class TrendingBlockItemPersonComponent extends TrendingBlockItemComponent {
    get imageSizes() {
        return this.imagesConfigs.profile_sizes;
    }

    get imagesPaths() {
        return {
            s: `${this.baseUrl}${this.imageSizes[1]}${this.args.data.profile_path}`,
            m: `${this.baseUrl}${this.imageSizes[2]}${this.args.data.profile_path}`,
            l: `${this.baseUrl}${this.imageSizes[3]}${this.args.data.profile_path}`
        };
    }
}
