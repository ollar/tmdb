import Model, { hasMany } from "@ember-data/model";

export default class MovieSimilarModel extends Model {
    @hasMany("movie", {
        async: false
    })
    results;
}
