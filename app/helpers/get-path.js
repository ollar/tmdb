import { helper } from "@ember/component/helper";
import ENV from "tmdb/config/environment";
const { rootURL } = ENV;

export default helper(function getPath(params /*, hash*/) {
    return rootURL + params.join("/");
});
