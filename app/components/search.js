import Component from "@glimmer/component";
import { inject as service } from "@ember/service";
import { tracked } from "@glimmer/tracking";
import { action } from "@ember/object";
import { task, timeout } from "ember-concurrency";
import { later } from "@ember/runloop";

export default class SearchComponent extends Component {
    @tracked query = "";
    @tracked inputFocused = false;

    @service search;
    @service router;
    @service tmdbConfiguration;

    // @readOnly("search.results") results;
    get results() {
        return this.search.results.slice(0, 5);
    }

    get shouldShowSuggestions() {
        return !this.args.hideAutosuggestions && this.inputFocused;
    }

    @task({
        maxConcurrency: 1,
        restartable: true,
    })
    *makeSearch(query) {
        yield timeout(400);
        yield this.search.fetch(query);
    }

    @action
    mutQuery(e) {
        e.preventDefault();
        this.query = e.target.value;
        this.makeSearch.perform(this.query);
    }

    @action
    goToSearch(e) {
        e.preventDefault();
        this.makeSearch.perform(this.query);
        this.router.transitionTo("index.search");
    }

    @action
    setInputFocused(focused) {
        later(() => {
            this.inputFocused = focused;
        }, 200);
    }
}
