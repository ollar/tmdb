import RestSerializer, {
    EmbeddedRecordsMixin
} from "@ember-data/serializer/rest";
import { normalizeModelName } from "@ember-data/store";

const RestSerializerEmbed = RestSerializer.extend(EmbeddedRecordsMixin);

export default class ApplicationSerializer extends RestSerializerEmbed {
    get appAdapter() {
        return this.store.adapterFor("application");
    }

    normalizeSingleResponse(
        store,
        primaryModelClass,
        payload,
        id,
        requestType
    ) {
        const _payload = {
            [primaryModelClass.modelName]: payload
        };

        return super.normalizeSingleResponse(
            store,
            primaryModelClass,
            _payload,
            id,
            requestType
        );
    }

    modelNameFromPayloadKey(key) {
        switch (key) {
            case "movie/credits":
            case "movie/recommendations":
            case "tv/credits":
            case "tv/recommendations":
                return normalizeModelName(key);

            default:
                return super.modelNameFromPayloadKey(key);
        }
    }
}
