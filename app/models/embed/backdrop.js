import Model, { attr } from '@ember-data/model';

export default class BackdropModel extends Model {
    @attr aspect_ratio;
    @attr height;
    @attr vote_average;
    @attr iso_639_1;
    @attr vote_count;
    @attr width;
}
