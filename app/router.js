import EmberRouter from '@ember/routing/router';
import config from 'tmdb/config/environment';

export default class Router extends EmberRouter {
    location = config.locationType;
    rootURL = config.rootURL;
}

Router.map(function() {
    this.route("index", { path: "/" }, function() {
      this.route("index", { path: "/" });
      this.route("movie", { path: "movies/:movie_id" });
      this.route("tv", { path: "tv/:tv_id" });
      this.route('search');
    });
    this.route("person", { path: "/persons/:person_id" });
});
