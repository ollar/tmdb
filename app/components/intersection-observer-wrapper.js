import Component from "@glimmer/component";
import { action } from "@ember/object";
import { later } from "@ember/runloop";

export default class IntersectionObserverWrapperComponent extends Component {
    rootMargin = "0px";
    threshold = 0.2;
    prevRatio = 0.5;

    @action
    initIntersectionObserver(target) {
        const options = {
            root: this.args.root && document.querySelector(this.args.root),
            rootMargin: this.args.rootMargin || this.rootMargin,
            threshold: this.args.threshold || this.threshold,
        };

        this.observer = new IntersectionObserver(this.callback, options);
        later(() => {
            this.observer.observe(target);
        }, 500);
    }

    @action
    destroyIntersectionObserver() {
        this.observer.disconnect();
    }

    @action
    callback(entries) {
        entries.forEach((entry) => {
            if (entry.intersectionRatio > this.prevRatio) {
                if (this.args.onVisible) this.args.onVisible(entry.target);
            } else {
                if (this.args.onInvisible) this.args.onInvisible(entry.target);
            }

            this.prevRatio = entry.intersectionRatio;
        });
    }
}
