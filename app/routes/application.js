import Route from "@ember/routing/route";
import { inject as service } from "@ember/service";
import { getOwner } from "@ember/application";

import * as Sentry from "@sentry/browser";
import * as Integrations from "@sentry/integrations";

export default class ApplicationRoute extends Route {
    @service intl;

    get _sentryDsn() {
        return getOwner(this).application.sentryDsn;
    }

    get _appName() {
        return getOwner(this).application.name;
    }

    get _appVersion() {
        return getOwner(this).application.version;
    }

    beforeModel() {
        this.intl.setLocale("en-us");

        if (this._sentryDsn) {
            Sentry.init({
                dsn: this._sentryDsn,
                integrations: [new Integrations.Ember()],
                release: `${this._appName}@${this._appVersion}`
            });
        }
    }
}
