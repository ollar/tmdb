import ApplicationSerializer from "../application";

export default class MovieCreditsSerializer extends ApplicationSerializer {
    attrs = {
        cast: { embedded: "always" },
        crew: { embedded: "always" }
    };
}
