import { module, test } from "qunit";
import { setupRenderingTest } from "ember-qunit";
import { render } from "@ember/test-helpers";
import { hbs } from "ember-cli-htmlbars";

import ENV from "tmdb/config/environment";
const { rootURL } = ENV;

module("Integration | Helper | get-path", function(hooks) {
    setupRenderingTest(hooks);

    // Replace this with your real tests.
    test("it renders", async function(assert) {
        this.set("inputValue", "1234");

        await render(hbs`{{get-path inputValue}}`);

        assert.equal(this.element.textContent.trim(), rootURL + "1234");
    });
});
