import Service, { inject as service } from "@ember/service";
import { tracked } from "@glimmer/tracking";

export default class TrendingService extends Service {
    @service store;

    @tracked movie = [];
    @tracked tv = [];
    @tracked person = [];

    get appAdapter() {
        return this.store.adapterFor("application");
    }

    get prefix() {
        return `${this.appAdapter.host}/${this.appAdapter.namespace}`;
    }

    url(media_type, time_window) {
        return `${this.prefix}/trending/${media_type}/${time_window}`;
    }

    /**
     * Fetches trending info
     * @description Get the daily or weekly trending items. The daily trending list tracks items over the period of a day while items have a 24 hour half life. The weekly list tracks items over a 7 day period, with a 7 day half life.
     *
     * @param {String} media_type - media type (movie | tv | person)
     * @param {String} time_window - time interval (day | week)
     * @param {Object} [query] - request parameters
     *
     * @see https://developers.themoviedb.org/3/trending/get-trending
     *
     * @returns {Model[]}
     */
    fetch(media_type = "movie", time_window = "day", query = {}) {
        if (this[media_type].length) return Promise.resolve(this[media_type]);
        return this.appAdapter
            .ajax(this.url(media_type, time_window), "GET", {
                data: query
            })
            .then(resp => {
                resp.results.forEach(m => {
                    let model = this.store.push(
                        this.store.normalize(media_type, m)
                    );
                    this[media_type].pushObject(model);
                });

                return this[media_type];
            });
    }
}
