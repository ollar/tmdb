import ApplicationSerializer from "../application";

export default class PosterSerializer extends ApplicationSerializer {
    primaryKey = "file_path";
}
