import ApplicationSerializer from "../application";

export default class BackdropSerializer extends ApplicationSerializer {
    primaryKey = "file_path";
}
