import Model, { attr } from '@ember-data/model';

export default class VideoModel extends Model {
    @attr iso_639_1;
    @attr iso_3166_1;
    @attr key;
    @attr name;
    @attr site;
    @attr size;
    @attr type;
}
