import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Service | tmdb-configuration', function(hooks) {
  setupTest(hooks);

  // Replace this with your real tests.
  test('it exists', function(assert) {
    let service = this.owner.lookup('service:tmdb-configuration');
    assert.ok(service);
  });
});
