import ApplicationAdapter from '../application';

export default class TvCreditsAdapter extends ApplicationAdapter {
    buildURL(modelName, id /*, snapshot, requestType, query */) {
        let prefix = this.urlPrefix();
        return `${prefix}/tv/${id}/credits`;
    }
}
