import Route from "@ember/routing/route";
import { inject as service } from "@ember/service";
import { hash } from "rsvp";

export default class IndexRoute extends Route {
    @service tmdbConfiguration;

    model() {
        return hash({
            configuration: this.tmdbConfiguration.fetch()
        });
    }
}
