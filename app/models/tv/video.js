import Model, { hasMany } from "@ember-data/model";

export default class MovieVideoModel extends Model {
    @hasMany("embed/video", {
        async: false
    })
    results;
}
