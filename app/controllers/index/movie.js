import Controller from "@ember/controller";
import { readOnly } from "@ember/object/computed";
import { inject as service } from "@ember/service";
import { action } from "@ember/object";

export default class MovieController extends Controller {
    treshold = 128;

    @service tmdbConfiguration;
    @service colorthief;

    @readOnly("model.info") info;

    @readOnly("tmdbConfiguration.backdropPaths") backdropPaths;
    @readOnly("tmdbConfiguration.posterPaths") posterPaths;
    @readOnly("tmdbConfiguration.logoPaths") logoPaths;
    @readOnly("tmdbConfiguration.profilePaths") profilePaths;

    get backgroundColor() {
        return `rgb(${this.colorthief.red},${this.colorthief.green},${this.colorthief.blue})`;
    }

    get textColor() {
        return this.colorthief.luma >= this.treshold ? "black" : "white";
    }

    @action
    calculateImageColors(e) {
        this.colorthief.getColor(e.target);
    }
}
