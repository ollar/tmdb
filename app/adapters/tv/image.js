import ApplicationAdapter from '../application';

export default class TvImageAdapter extends ApplicationAdapter {
    buildURL(modelName, id /*, snapshot, requestType, query */) {
        let prefix = this.urlPrefix();
        return `${prefix}/tv/${id}/images`;
    }
}
