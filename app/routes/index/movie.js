import Route from "@ember/routing/route";
import { hash } from "rsvp";

export default class IndexMovieRoute extends Route {
    model({ movie_id }) {
        return hash({
            movie_id,
            info: super.model(...arguments)
            // credits: this.store.findRecord("movie/credits", movie_id),
            // images: this.store.findRecord("movie/image", movie_id),
            // videos: this.store.findRecord("movie/video", movie_id),
            // similar: this.store.findRecord("movie/similar", movie_id),
            // recommendations: this.store.findRecord(
            //     "movie/recommendations",
            //     movie_id
            // )
        });
    }
}
