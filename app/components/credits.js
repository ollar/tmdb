import Component from "@glimmer/component";
import { inject as service } from "@ember/service";
import { tracked } from "@glimmer/tracking";
import { task } from "ember-concurrency";
import { action } from "@ember/object";

export default class CreditsComponent extends Component {
    @service store;
    @tracked _credits = null;
    @tracked expanded = false;

    get credits() {
        if (!this._credits) return this._credits;
        return this.expanded
            ? this._credits
            : { cast: this._credits.cast.slice(0, 3) };
    }

    @task({ drop: true })
    *fetchCredits() {
        if (!this.args.type || !this.args.id) return;
        if (!this._credits) {
            this._credits = yield this.store.findRecord(
                this.args.type,
                this.args.id
            );
        }
    }

    @action
    onCreditsVisible() {
        this.fetchCredits.perform();
    }

    @action
    toggleCredits() {
        this.expanded = !this.expanded;
    }
}
