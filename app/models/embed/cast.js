import Model, { attr } from "@ember-data/model";

export default class CastModel extends Model {
    @attr cast_id;
    @attr character;
    @attr credit_id;
    @attr gender;
    @attr name;
    @attr order;
    @attr profile_path;
}
