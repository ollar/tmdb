import Model, { hasMany } from "@ember-data/model";

export default class TvSimilarModel extends Model {
    @hasMany("tv", {
        async: false
    })
    results;
}
