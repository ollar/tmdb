import Model, { hasMany } from "@ember-data/model";

export default class TvCreditsModel extends Model {
    @hasMany("embed/cast", {
        async: false
    })
    cast;
    @hasMany("embed/crew", {
        async: false
    })
    crew;
}
