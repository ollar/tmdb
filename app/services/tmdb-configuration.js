import Service, { inject as service } from "@ember/service";
import { tracked } from "@glimmer/tracking";
import { readOnly } from "@ember/object/computed";

export default class TmdbConfigurationService extends Service {
    @service store;
    @tracked model = null;

    @readOnly("images.secure_base_url") imagesBaseUrl;

    get adapter() {
        return this.store.adapterFor("application");
    }

    get ajax() {
        return this.adapter.ajax.bind(this.adapter);
    }

    get url() {
        return `${this.adapter.host}/${this.adapter.namespace}/configuration`;
    }

    get images() {
        if (!this.model) return {};

        return this.model.images;
    }

    get backdropPaths() {
        return {
            s: `${this.imagesBaseUrl}${this.images.backdrop_sizes[1]}`,
            m: `${this.imagesBaseUrl}${this.images.backdrop_sizes[2]}`,
            l: `${this.imagesBaseUrl}${this.images.backdrop_sizes[3]}`
        };
    }

    get posterPaths() {
        return {
            s: `${this.imagesBaseUrl}${this.images.poster_sizes[1]}`,
            m: `${this.imagesBaseUrl}${this.images.poster_sizes[2]}`,
            l: `${this.imagesBaseUrl}${this.images.poster_sizes[3]}`
        };
    }

    get logoPaths() {
        return {
            s: `${this.imagesBaseUrl}${this.images.logo_sizes[1]}`,
            m: `${this.imagesBaseUrl}${this.images.logo_sizes[2]}`,
            l: `${this.imagesBaseUrl}${this.images.logo_sizes[3]}`
        };
    }

    get profilePaths() {
        return {
            s: `${this.imagesBaseUrl}${this.images.profile_sizes[1]}`,
            m: `${this.imagesBaseUrl}${this.images.profile_sizes[2]}`,
            l: `${this.imagesBaseUrl}${this.images.profile_sizes[3]}`
        };
    }

    fetch() {
        if (this.model) return Promise.resolve(this.model);

        return this.ajax(this.url, "GET", {}).then(resp => {
            this.model = resp;
            return resp;
        });
    }
}
