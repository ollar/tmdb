import Component from "@glimmer/component";
import { action } from "@ember/object";
import Swiper from "swiper";

export default class SwiperContainerComponent extends Component {
    swiper = null;

    @action
    initializeSwiper(el) {
        this.swiper = new Swiper(el, this.args.swiperOptions);
    }

    @action
    updateSwiper() {
        // this.swiper.update();
        this.destroySwiper(...arguments);
        this.initializeSwiper(...arguments);
    }

    @action
    destroySwiper() {
        if (this.swiper) this.swiper.destroy();
    }
}
