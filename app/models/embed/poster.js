import Model, { attr } from '@ember-data/model';

export default class PosterModel extends Model {
    @attr aspect_ratio;
    @attr height;
    @attr iso_639_1;
    @attr vote_average;
    @attr vote_count;
    @attr width;
}
