import { module, test } from "qunit";
import { visit, currentURL, waitFor } from "@ember/test-helpers";
import { setupApplicationTest } from "ember-qunit";

module("Acceptance | index", function(hooks) {
    setupApplicationTest(hooks);

    test("visiting /index", async function(assert) {
        await visit("/");

        assert.equal(currentURL(), "/");
    });

    test("Index has trending block", async function(assert) {
        await visit("/");

        await waitFor("[data-test-trending-block]");

        assert.equal(currentURL(), "/");
    });
});
