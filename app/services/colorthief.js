import Service from '@ember/service';
import ColorThief from 'colorthief';
import { tracked } from "@glimmer/tracking";

export default class ColorthiefService extends Service {
    constructor() {
        super(...arguments);
        this.colorThief = new ColorThief();
    }

    @tracked red = 255;
    @tracked green = 255;
    @tracked blue = 255;

    get luma() {
        return 0.2126 * this.red + 0.7152 * this.green + 0.0722 * this.blue;
    }

    /**
     * Gets the dominant color from the image. Color is returned as an array of three integers representing red, green, and blue values.
     *
     * @param {HTMLElement} image - When called in the browser, this argument expects an HTML image element, not a URL. When run in Node, this argument expects a path to the image.
     * @param {Number} quality - is an optional argument that must be an Integer of value 1 or greater, and defaults to 10. The number determines how many pixels are skipped before the next one is sampled. We rarely need to sample every single pixel in the image to get good results. The bigger the number, the faster a value will be returned.
     * @returns {[Number, Number, Number]}
     */
    getColor(image, quality = 10) {
        [this.red, this.green, this.blue] = this.colorThief.getColor(image, quality);
        return [this.red, this.green, this.blue];
    }

    /**
     * Gets a palette from the image by clustering similar colors. The palette is returned as an array containing colors, each color itself an array of three integers.
     *
     * @param {HTMLElement} image
     * @param {Number} colorCount
     * @param {Number} quality
     * @returns {[[Number, Number, Number], ...]}
     */
    getPalette(image, colorCount = 10, quality = 10) {
        return this.getPalette.getColor(image, colorCount, quality);
    }
}
